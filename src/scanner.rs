use {lib::ParseError, std::fs::File, std::io::prelude::*, std::iter::Peekable,
     std::str::{Chars, FromStr}, token::{reg_name, reserved_word, Token, TokenType}};

#[derive(Debug)]
pub struct Scanner {
    stream: String,
    tokens: Vec<Token>,
    line: u32,
}

impl Scanner {
    pub fn new(input_file: &mut File) -> Scanner {
        let mut input_stream = String::new();
        input_file.read_to_string(&mut input_stream).unwrap();
        Scanner {
            line: 1,
            stream: input_stream,
            tokens: Vec::new(),
        }
    }

    /// Scans (and takes ownership of the scnaner) the input
    /// On success returns the completed token stream for the Parser's pass.
    /// On error, returns a String of the error message
    pub fn scan(mut self) -> Result<Vec<Token>, ParseError> {
        use self::TokenType::COMMA;
        let mut stream = self.stream.chars().peekable();
        while let Some(c) = stream.next() {
            match c {
                ',' => self.tokens
                    .push(Token::new(COMMA, String::from(","), self.line)),
                '\n' => self.line += 1,
                '$' => {
                    let tok = self.reg(&mut stream)?;
                    self.tokens.push(tok);
                }
                'a'...'z' | 'A'...'Z' => {
                    let tok = self.ident_label(c, &mut stream)?;
                    self.tokens.push(tok);
                }
                '0'...'9' => {
                    let tok = self.number(c, &mut stream)?;
                    self.tokens.push(tok);
                }
                ' ' => continue,
                _ => {
                    return Err(ParseError::new(
                        self.line,
                        String::from("Unexpected character"),
                        format!("{}", c),
                    ))
                }
            }
        }
        Ok(self.tokens)
    }

    /// Peek, unwrap and dereference (copy) the reference to the char this is due to
    /// borrows in a while let not ending until the loop is over and we want to advance
    /// the iterator(borrow mutibly) inside the while loop.
    fn deref_peek(stream: &mut Peekable<Chars>) -> Option<char> {
        let peeked = stream.peek();
        if peeked.is_none() {
            None
        } else {
            Some(*peeked.unwrap())
        }
    }

    /// Helper method to scan for an identifier or a label, which is just an identifier
    /// declared with a colon afterwards.
    ///
    /// The method will read until a ':' or a non-alphanumeric. Once the full identifier
    /// has been seen, it will check to see if it is a reserved word. Error if it is
    /// a Label with a reserved word's name.
    ///
    /// On success, will return the token to add to the tokens vector, on error returns and
    /// error message.
    fn ident_label(&self, c: char, stream: &mut Peekable<Chars>) -> Result<Token, ParseError> {
        let mut identifier = String::new();
        let mut is_label = false;
        identifier.push(c);

        while let Some(c) = Scanner::deref_peek(stream) {
            match c {
                '0'...'9' | 'A'...'Z' | 'a'...'z' => {
                    if let Some(c) = stream.next() {
                        identifier.push(c);
                    }
                }
                ':' => {
                    if let Some(c) = stream.next() {
                        identifier.push(c);
                        is_label = true;
                        break;
                    }
                }
                _ => break,
            }
        }
        use self::TokenType::{LABEL, IDENT};
        if is_label {
            if let Some(_) = reserved_word(&identifier[..identifier.len() - 1]) {
                Err(ParseError::new(
                    self.line,
                    String::new(),
                    format!(
                        "Cannot have label with reserved name {}",
                        &identifier[..identifier.len() - 1]
                    ),
                ))
            } else {
                Ok(Token::new(LABEL, identifier, self.line))
            }
        } else if let Some(reserved_token_type) = reserved_word(&identifier) {
            Ok(Token::new(reserved_token_type, identifier, self.line))
        } else {
            Ok(Token::new(IDENT, identifier, self.line))
        }
    }

    fn number(&self, c: char, stream: &mut Peekable<Chars>) -> Result<Token, ParseError> {
        let mut lexeme = String::new();
        lexeme.push(c);
        while let Some(c) = Scanner::deref_peek(stream) {
            match c {
                '0'...'9' => {
                    if let Some(c) = stream.next() {
                        lexeme.push(c);
                    }
                }
                _ => break,
            }
        }
        use self::TokenType::NUMBER;
        match u32::from_str(&lexeme) {
            Ok(val) => Ok(Token::new(NUMBER(val), lexeme, self.line)),
            Err(e) => Err(ParseError::new(self.line, String::new(), format!("{}", e))),
        }
    }

    fn reg(&self, stream: &mut Peekable<Chars>) -> Result<Token, ParseError> {
        let mut lexeme = String::new();
        lexeme.push('$');
        while let Some(c) = Scanner::deref_peek(stream) {
            match c {
                'A'...'Z' | 'a'...'z' | '0'...'9' => {
                    if let Some(c) = stream.next() {
                        lexeme.push(c)
                    }
                }
                _ => break,
            }
        }
        use self::TokenType::REG;
        if let Some(_) = reg_name(&lexeme) {
            Ok(Token::new(REG, lexeme, self.line))
        } else {
            Err(ParseError::new(
                self.line,
                String::from("Invalid register name"),
                lexeme,
            ))
        }
    }
}
