extern crate argparse;
#[macro_use]
extern crate lazy_static;
extern crate regex;

pub mod lib;
pub mod scanner;
pub mod token;

use {argparse::{ArgumentParser, Store}, lib::report_exit, scanner::Scanner, std::fs::File};

fn main() {
    let mut infilename = String::new();
    let mut outfilename = String::new();
    {
        let mut ap = ArgumentParser::new();
        ap.set_description("MIPS Assembler");
        ap.refer(&mut infilename)
            .required()
            .add_option(&["-i"], Store, "Input assembly file");
        ap.refer(&mut outfilename)
            .required()
            .add_option(&["-o"], Store, "Output binary file");
        ap.parse_args_or_exit();
    }
    let mut infile = File::open(infilename).unwrap();
    let tokens = match Scanner::new(&mut infile).scan() {
        Ok(tokens) => tokens,
        Err(e) => report_exit(e),
    };
    for tok in tokens.iter() {
        println!("{:?}", tok);
    }
}
