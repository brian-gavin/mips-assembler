#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum InstructionType {
    R(u8),
    I(u8),
    J(u8),
    None,
}

pub struct Instruction {
    in_type: InstructionType,
    rs: u8,
    rt: u8,
    rd: u8,
    immed: u16,
    jump: u32,
}

pub struct InstructionBuilder {
    in_type: InstructionType,
    rs: u8,
    rt: u8,
    rd: u8,
    immed: u16,
    jump: u32,
}

impl Instruction {
    fn encode_rtype(&self, function: u8) -> u32 {
        let mut ret: u32 = 0;
        ret |= (self.rs as u32) << (32 - 10);
        ret |= (self.rt as u32) << (32 - 15);
        ret |= (self.rd as u32) << (32 - 20);
        ret |= function as u32;
        ret
    }

    fn encode_itype(&self, opcode: u8) -> u32 {
        let mut ret: u32 = 0;
        ret |= (opcode as u32) << (32 - 5);
        ret |= (self.rs as u32) << (32 - 10);
        ret |= (self.rt as u32) << (32 - 15);
        ret |= self.immed as u32;
        ret
    }

    fn encode_jtype(&self, opcode: u8) -> u32 {
        let mut ret: u32 = 0;
        ret |= (opcode as u32) << (32 - 5);
        ret |= self.jump;
        ret
    }

    pub fn encode(&self) -> u32 {
        let ret: u32;
        match self.in_type {
            InstructionType::R(function) => ret = self.encode_rtype(function),
            InstructionType::I(opcode) => ret = self.encode_itype(opcode),
            InstructionType::J(opcode) => ret = self.encode_jtype(opcode),
            InstructionType::None => panic!("None type instruction"),
        }
        ret
    }
}

impl Default for InstructionBuilder {
    fn default() -> InstructionBuilder {
        InstructionBuilder {
            in_type: InstructionType::None,
            rs: 0,
            rt: 0,
            rd: 0,
            immed: 0,
            jump: 0,
        }
    }
}

impl InstructionBuilder {
    pub fn new(t: InstructionType) -> InstructionBuilder {
        InstructionBuilder {
            in_type: t,
            ..Default::default()
        }
    }

    pub fn rs(&mut self, rs: u8) -> &mut InstructionBuilder {
        self.rs = rs;
        self
    }

    pub fn rd(&mut self, rd: u8) -> &mut InstructionBuilder {
        self.rd = rd;
        self
    }

    pub fn rt(&mut self, rt: u8) -> &mut InstructionBuilder {
        self.rt = rt;
        self
    }

    pub fn immed(&mut self, immed: u16) -> &mut InstructionBuilder {
        self.immed = immed;
        self
    }

    pub fn jump(&mut self, jump: u32) -> &mut InstructionBuilder {
        self.jump = jump;
        self
    }

    pub fn build(&self) -> Instruction {
        Instruction {
            in_type: self.in_type,
            rs: self.rs,
            rt: self.rt,
            rd: self.rd,
            immed: self.immed,
            jump: self.jump,
        }
    }
}
