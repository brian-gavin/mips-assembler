use regex::Regex;

#[derive(Debug)]
pub enum TokenType {
    // Instructions
    ADD,
    ADDI,
    ADDIU,
    ADDU,
    AND,
    ANDI,
    BEQ,
    BNE,
    J,
    JAL,
    JR,
    LBU,
    LHU,
    LL,
    LUI,
    LW,
    NOR,
    OR,
    ORI,
    SLT,
    SLTI,
    SLTIU,
    SLTU,
    SLL,
    SRL,
    SB,
    SC,
    SH,
    SW,
    SUB,
    SUBU,
    // Special Identifiers
    REG,
    IDENT,
    LABEL,
    NUMBER(u32),

    COMMA,
}

#[derive(Debug)]
pub struct Token {
    ttype: TokenType,
    lexeme: String,
    line: u32,
}

impl Token {
    pub fn new(ttype: TokenType, lexeme: String, line: u32) -> Token {
        Token {
            ttype: ttype,
            lexeme: lexeme,
            line: line,
        }
    }
}

pub fn reserved_word(token: &str) -> Option<TokenType> {
    use self::TokenType::*;
    match token {
        "add" => Some(ADD),
        "addi" => Some(ADDI),
        "addiu" => Some(ADDIU),
        "addu" => Some(ADDU),
        "and" => Some(AND),
        "andi" => Some(ANDI),
        "beq" => Some(BEQ),
        "bne" => Some(BNE),
        "j" => Some(J),
        "jal" => Some(JAL),
        "jr" => Some(JR),
        "lbu" => Some(LBU),
        "lhu" => Some(LHU),
        "ll" => Some(LL),
        "lui" => Some(LUI),
        "lw" => Some(LW),
        "nor" => Some(NOR),
        "or" => Some(OR),
        "ori" => Some(ORI),
        "slt" => Some(SLT),
        "slti" => Some(SLTI),
        "sltiu" => Some(SLTIU),
        "sltu" => Some(SLTU),
        "sll" => Some(SLL),
        "srl" => Some(SRL),
        "sb" => Some(SB),
        "sc" => Some(SC),
        "sh" => Some(SH),
        "sw" => Some(SW),
        "sub" => Some(SUB),
        "subu" => Some(SUBU),
        _ => None,
    }
}

pub fn reg_name(token: &str) -> Option<&str> {
    lazy_static! {
        static ref REG_REGEX: Regex = Regex::new(
            r"\$((3[0-1])|(2\d)|(1\d)|(\d)|(zero|at|gp|sp|fp|ra)|(t[0-9])|(s[0-7])|(v[0-1])|(a[0-3])|(k[0-1]))"
        ).unwrap();
    }
    if REG_REGEX.is_match(token) {
        Some(token)
    } else {
        None
    }
}
