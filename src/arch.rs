use instruction::InstructionType;
use instruction::InstructionType::I;
use instruction::InstructionType::J;
use instruction::InstructionType::R;
use std::collections::HashMap;

pub type OpcodeMap = HashMap<&'static str, InstructionType>;

static OPCODE_PAIRS: &'static [(&'static str, InstructionType)] = &[
    ("add", R(0x20)),
    ("addi", I(0x8)),
    ("addiu", I(0x9)),
    ("addu", R(0x21)),
    ("and", R(0x24)),
    ("andi", I(0xc)),
    ("beq", I(0x4)),
    ("bne", I(0x5)),
    ("j", J(0x2)),
    ("jal", J(0x3)),
    ("jr", R(0x8)),
    ("lbu", I(0x24)),
    ("lhu", I(0x25)),
    ("ll", I(0x30)),
    ("lui", I(0xf)),
    ("lw", I(0x23)),
    ("nor", R(0x27)),
    ("or", R(0x25)),
    ("ori", I(0xd)),
    ("slt", R(0x2a)),
    ("slti", I(0xa)),
    ("sltiu", I(0xb)),
    ("sltu", R(0x2b)),
    ("sll", R(0x00)),
    ("srl", R(0x2)),
    ("sb", I(0x28)),
    ("sc", I(0x38)),
    ("sh", I(0x29)),
    ("sw", I(0x2b)),
    ("sub", R(0x22)),
    ("subu", R(0x23)),
];

static REGISTERS: &'static [&'static str] = &[
    "zero", "at", "v0", "v1", "a0", "a1", "a2", "a3", "t0", "t1", "t2", "t3", "t4", "t5", "t6",
    "t7", "s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7", "t8", "t9", "k0", "k1", "gp", "sp", "fp",
    "ra",
];

pub fn build_assemble_map() -> OpcodeMap {
    let mut map = OpcodeMap::new();
    for &(op, itype) in OPCODE_PAIRS {
        map.insert(op, itype);
    }
    map
}
