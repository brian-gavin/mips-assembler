use {std::fmt, std::process};

pub struct ParseError {
    line: u32,
    etype: String,
    msg: String,
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{}] Error {}: {}", self.line, self.etype, self.msg)
    }
}

impl ParseError {
    pub fn new(line: u32, etype: String, msg: String) -> ParseError {
        ParseError {
            line: line,
            etype: etype,
            msg: msg,
        }
    }

    pub fn report(&self) {
        eprintln!("{}", self);
    }
}

pub fn report_exit(e: ParseError) -> ! {
    e.report();
    process::exit(0)
}

//const INSTRUCTIONS: &'static [&'static str] = &[
/*
("addi", ADDI),
("addiu", ADDIU),
("addu", ADDU),
("and", AND),
("andi", ANDI),
("beq", BEQ),
("bne", BNE),
("j", J),
("jal", JAL),
("jr", JR),
("lbu", LBU),
("lhu", LHU),
("ll", LL),
("lui", LUI),
("lw", LW),
("nor", NOR),
("or", OR),
("ori", ORI),
("slt", SLT),
("slti", SLTI),
("sltiu", SLTIU),
("sltu", SLTU),
("sll", SLL),
("srl", SRL),
("sb", SB),
("sc", SC),
("sh", SH),
("sw", SW),
("sub", SUB),
("subu", SUBU)
*/
